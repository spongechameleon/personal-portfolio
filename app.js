const pictures = document.querySelectorAll(".picture");
const imgs = document.querySelectorAll(".picture a img");

// touch overlay substitute for ".box:hover .picture" effects
let i;
for (i = 0; i < pictures.length; ++i) {
  pictures[i].addEventListener("touchstart", showPictureOverlayTouch);
}

// navigate to the parent anchor href when overlay img is touched
let k;
for (k = 0; k < imgs.length; ++k) {
  imgs[k].addEventListener("touchstart", function (event) {
    window.location.href = event.target.parentElement.attributes.href.value;
  });
}

// remove project overlay on non-selected .picture elements
let pic, a;
document.body.addEventListener("touchstart", function (event) {
  for (pic of pictures) {
    // clear overlay, if not picture or icons in existing overlay
    if (
      pic.id !== event.target.id &&
      !event.target.classList.contains("icon")
    ) {
      pic.classList.remove("overlay");
      for (a of pic.children) {
        // hide img tag
        a.children[0].style.display = "none";
      }
    }
  }
});

function showPictureOverlayTouch(event) {
  // no auto-clicking the anchor tag in the overlay!!
  event.preventDefault();
  let a;
  // change background of div.picture, don't add class to icons in overlay
  if (event.target.classList.contains("picture")) {
    event.target.classList.add("overlay");
    for (a of event.target.children) {
      // display img inside anchor inside div.picture
      a.children[0].style.display = "inline-block";
    }
  }
}

// copy to clipboard
const emailElement = document.querySelector("#email p"),
  clipboardBtn = document.querySelector("#email button"),
  tooltipText = document.querySelector(".tooltiptext");

clipboardBtn.addEventListener("click", copyToClipboard);
clipboardBtn.addEventListener(
  "mouseenter",
  () => (tooltipText.textContent = "Copy email"),
);

function copyToClipboard() {
  const el = document.createElement("textarea");
  el.value = emailElement.textContent;
  document.body.appendChild(el);
  el.select();
  document.execCommand("copy");
  document.body.removeChild(el);
  tooltipText.textContent = "Got it!";
}
